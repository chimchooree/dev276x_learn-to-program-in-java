import java.util.Scanner;

public class DEV276x_Crypto {
    private static Scanner input = new Scanner(System.in);

    private static String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main (String [] args) {
        int KEY = 1;
        int GROUP = 4;
        System.out.print("Enter code, and I'll encrypt it a little: ");
        String raw = input.nextLine();
        System.out.println("Encrypted Message: ");
        String encrypted = encryptString(raw, KEY, GROUP);
        System.out.println(encrypted);
        System.out.println("Now I'll decrypt it a little:");
        String decrypted = decryptString(encrypted, KEY);
        System.out.print(decrypted);
    }

    // Remove all spaces and punctuation. Turn all lower-case letters into upper-case letters.
    public static String normalize(String raw) {
        raw = raw.replaceAll("\\p{Punct}", "");
        raw = raw.replaceAll("\\p{Digit}", "");
        raw = raw.replaceAll("\\p{Blank}", "");
        raw = raw.toUpperCase();
        return raw;
    }

    //Shift each letter in the input string forward by the key.
    public static String caesarify(String raw, int key) {
        String shifted = shiftAlphabet(key);
        char[] temp = raw.toCharArray();
        for (int i = 0; i < temp.length; i++)
            temp[i] = shifted.charAt(ALPHABET.indexOf(temp[i]));

        return new String (temp);
    }

    // Function provided by the instructor
    // Shift letters in alphabet over by given integer
    public static String shiftAlphabet(int shift) {
        int start = 0;
        if (shift < 0) {
            start = (int) 'Z' + shift + 1;
        } else {
            start = 'A' + shift;
        }
        String result = "";
        char currChar = (char) start;
        for(; currChar <= 'Z'; ++currChar) {
            result = result + currChar;
        }
        if(result.length() < 26) {
            for(currChar = 'A'; result.length() < 26; ++currChar) {
                result = result + currChar;
            }
        }
        return result;
    }

    // Breaks the message into groups of an equal number of characters. Pad out the last group with x's.
    public static String groupify(String raw, int group) {

        String out = "";
        int length = raw.length();
        int spaces = 0;

            for (var i = 0; i < length; ++i) {
                out += raw.charAt(i);
                if (i != 0 && i != 1 && (i + 1) % group == 0 && i != length - 1) {
                    out += " ";
                    spaces += 1;
                }
            }

        out = addX(out, group, spaces);
        return out;
    }

    public static String addX(String out, int group, int spaces) {
        int remainder;
        if (out.length() < group) {
            remainder = group - out.length();
        } else {
            int temp = out.length() - (spaces*group + spaces);
            remainder = group - temp;
            if (remainder == 4) {
                remainder = 0;
            }
        }
        for (int j = 0; j < remainder; ++j) {
            out += "x";
        }
        return out;
    }


    public static String encryptString(String raw, int key, int group) {
        return groupify(caesarify(normalize(raw), key), group);
    }

    // Return message without spaces
    public static String ungroupify(String raw)
    {
        raw = raw.replaceAll(" ", "");
        raw = raw.replaceAll("x", "");
        return raw;
    }

    // Return unencrypted text
    public static String decryptString(String raw, int key)
    {
        raw = ungroupify(raw);
        String shifted = shiftAlphabet(0 - key);
        char[] message = raw.toCharArray();
        for (int i = 0; i < message.length; i++) {
            message[i] = shifted.charAt(ALPHABET.indexOf(message[i]));
        }
        return new String (message);
    }
}
