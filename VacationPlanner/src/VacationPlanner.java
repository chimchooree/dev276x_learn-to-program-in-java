import java.util.Scanner;

/*
    Vacation Planner uses simple questions to provide basic information about your trip.
 */

public class VacationPlanner {
    private static int HOURS_IN_DAY = 24;
    private static int MINUTES_IN_HOUR = 60;
    private static Scanner listener = new Scanner(System.in);

    public static void main (String[] args) {
        System.out.println("Welcome to Vacation Planner!");
        introduceUser(); // Part 1
        separateSection();
        learnBudget(); // Part 2
        separateSection();
        findTimeDifference(); // Part 3
        separateSection();
        calculateArea(); // Part 4
        separateSection();
        learnDistance(); // Bonus
        separateSection();
        System.out.println("All done! Bon voyage.");
    }

    // Print a border to mark new section
    public static void separateSection() {
        System.out.println("***********");
        System.out.println("");
    }

    // Learn basic information
    public static void introduceUser() {
        System.out.print("What is your name? " );
        String name = listener.nextLine();
        System.out.print("Nice to meet you, " + name + ". Where are you traveling to? ");
        String destination = listener.nextLine();
        System.out.println("Great! " + destination + " sounds like a great trip");
    }

    // Learn budget information
    public static void learnBudget() {
        int tripLength;
        do {
            System.out.print("How many days are you going to spend traveling? ");
            tripLength = chooseInt();
        } while (!checkInt(tripLength));

        System.out.print("How much money, in USD, are you planning to spend on your trip? ");
        double totalBudget = listener.nextDouble();
        do {
            System.out.print("How much money, in USD, are you planning to spend on your trip? ");
            totalBudget = chooseDouble();
        } while (!checkDouble(totalBudget));

        System.out.print("What is the three letter currency symbol for your travel destination? ");
        String currency = listener.next().toUpperCase();

        double exchangeRate;
        do {
            System.out.print("How many " + currency + " are there in 1 USD? ");
            exchangeRate = chooseDouble();
        } while (!checkDouble(exchangeRate) || exchangeRate <= 0);

        System.out.println("");
        System.out.println("If you are traveling for " + tripLength + " days, that is the same as " +
                calculateHour(tripLength) + " hours or " + calculateMinute(tripLength) + " minutes or " +
                calculateSecond(tripLength) + " seconds.");
        System.out.println("If you are going to spend " + totalBudget + " USD that means you can spend about " +
                calculateDailyBudget(totalBudget, tripLength) + " USD per day");
        double newBudget = calculateCurrency(totalBudget, exchangeRate);
        System.out.println("Your total budget in " + currency + " is " + newBudget + ", which is " +
                calculateDailyBudget(newBudget, tripLength) + " per day.");
    }

    // Round given value to 2 decimal places
    public static double roundValue(double value) {
        double newValue = (int)(value * 100); // in "cents"
        newValue = (double)newValue/100.0; // in "dollars"
        return newValue;
    }

    // Estimate hours in vacation
    public static int calculateHour(int day) {
        return day * HOURS_IN_DAY;
    }

    // Estimate minutes in vacation
    public static int calculateMinute(int day) {
        return day * HOURS_IN_DAY * MINUTES_IN_HOUR;
    }

    // Estimate seconds in vacation
    public static int calculateSecond(int day) {
        return day * HOURS_IN_DAY * MINUTES_IN_HOUR * 60;
    }

    // Estimate daily budget for vacation
    public static double calculateDailyBudget(double budget, int days) {
        double dailyBudget = budget / days;
        return roundValue(dailyBudget);
    }

    // Estimate given amount of money's value after a given exchange rate
    public static double calculateCurrency(double budget, double rate) {
        return roundValue(budget * rate);
    }

    // Learn the time difference between home and destination
    public static void findTimeDifference() {
        int MIDNIGHT = 24;
        int NOON = 12;

        System.out.print("What is the time difference, in hours, between your home and your destination? ");
        int timeDifference = listener.nextInt();
        System.out.println("That means that when it is midnight at home, it will be " +
                findNewTime(MIDNIGHT, timeDifference) + ":00 in your travel destination. When it is noon at home, it " +
                "will be " + findNewTime(NOON, timeDifference) + ":00");
    }

    // Find destination's time when it's a given time at home
    public static int findNewTime(int oldTime, int difference) {
        return java.lang.StrictMath.abs((oldTime + difference) % HOURS_IN_DAY);
    }

    // Find destination's square mileage
    public static void calculateArea() {
        System.out.print("What is the area of your destination country in square kilometers? ");
        double area = listener.nextDouble();
        System.out.println("In square miles, that is " + calculateMiles(area));
    }

    // Convert sq km to sq mi
    public static double calculateMiles(double km) {
        double SQ_KM_TO_SQ_MI = 2.59;
        return roundValue(km / SQ_KM_TO_SQ_MI);
    }

    // Learn Distance to Destination
    public static void learnDistance() {
        System.out.println("Next, let's find the distance between home and your travel destination.");
        System.out.print("What is your home's latitude? ");
        double homeLat = listener.nextDouble();
        System.out.print("What is your home's longitude? ");
        double homeLon = listener.nextDouble();
        System.out.print("What is your destination's latitude? ");
        double destLat = listener.nextDouble();
        System.out.print("What is your destination's longitude? ");
        double destLon = listener.nextDouble();
        System.out.println("");
        System.out.println("The distance to your travel destination is about " + calculateDistance(homeLat, homeLon, destLat, destLon) + "km");
    }

    // Calculate Distance to Destination
    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        int EARTH_RADIUS = 6371; //km

        double latRad = Math.toRadians(lat2 - lat1);
        double lonRad = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(latRad/2), 2) + Math.pow(Math.sin(lonRad/2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return roundValue(EARTH_RADIUS * c);
    }

    public static int chooseInt() {
        try {
            return Integer.parseInt(listener.next());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static boolean checkInt(int number) {
        return number >= 0;
    }

    public static double chooseDouble() {
        try {
            return Double.parseDouble(listener.next());
        } catch (NumberFormatException e) {
            return -1.0;
        }
    }

    public static boolean checkDouble(double number) {
        return number >= 0.0;
    }
}