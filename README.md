# DEV276x - Learn to Program in Java

My solution for all projects in edx's Java intro course.

## Project Details

* **MOOC site:** courses.edx.org
* **Course:** DEV276x: Learn to Program in Java
* **Link:** https://courses.edx.org/courses/course-v1:Microsoft+DEV276x+2T2019/courseware/

## Module 1: Java Basics

* **Project:** Vacation Planner
* **Skills:** User input from Scanner, math, print, import

## Module 2: Control Structures

* **Project:** Odds or Evens
* **Skills:** Loops and conditionals (`do`, `while`, `if`, `else if`, `else`), `try` and `catch`, exceptions, switch, random numbers

## Module 3: Data Flow

* **Project:** Crypto
* **Skills:** Recursion

## Module 4: Final Project

* **Project:** Maze Runner