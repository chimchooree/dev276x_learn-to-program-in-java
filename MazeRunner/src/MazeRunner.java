/*
    DEV276x - Learn to Program in Java
    Final Project - Maze Runner
    https://courses.edx.org/courses/course-v1:Microsoft+DEV276x+2T2019/courseware/72c3ae7f7e2f4f54ae5d6d3cfad37409/e14fa96703cb433e99a023799356bb14/1?activate_block_id=block-v1%3AMicrosoft%2BDEV276x%2B2T2019%2Btype%40html%2Bblock%40afc69ecdbd6f4d7a9628c95470b41e63
 */

/*
        // PART 1
        //intro(): welcome user to Maze Runner and show them the current state of the Maze.
        //"Welcome to Maze Runner!
        //Here is your current position:
        //........
        //x......
        //......
        myMap.printMap();
        //x is current position. . is an unknown space and will turn into walls - or free spaces * or more.
        // Tell user to move with R L U or D (More intuitive than WASD, I guess?)
        //userMove(): Where would you like to move? (R, L, U, D) [input.next]
        //return a string indicating which direction the user chooses to move in. this method should guarantee that the user selection is only one of the given options, and continue to reprompt the user until they enter a desired direction.
        // make sure you can move that way according to the map.
        myMap.canIMoveRight(); //return true if free or false if wall
        myMap.canIMoveLeft();
        myMap.canIMoveUp();
        myMap.canIMoveDown();
        // if false, notify user there is a wall in that direction and ask them to pick a new direction.
        // "sorry you've hit a wall
        // if true, move there
        myMap.moveRight();
        myMap.moveLeft();
        myMap.moveUp();
        myMap.moveDown();
        // print current state of the map
        // repeat until you win. You win when myMap.didIWin == true
        // print "Congratz, you made it out alive!"
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MazeRunner {
    private static Scanner input = new Scanner(System.in); // user input
    private static Maze myMap = new Maze();
    private static ArrayList directions = new ArrayList<>(Arrays.asList("r", "l", "u", "d"));

    // Main
    public static void main(String[] args) {
        intro();
        userMove();
    }

    // Welcome User and show initial map & position
    public static void intro() {
        System.out.println("Welcome to Maze Runner!");
        System.out.println("Here is your current position:");
        myMap.printMap();
    }

    // Victory Message
    public static void win() {
        System.out.println("You escaped the Maze! Congratulations.");
    }

    // Manage User Movement
    public static void userMove() {
        // Prompt and save User input for desired movement direction
        System.out.print("Where would you like to move? (R, L, U, D");
        String direction = input.next();
        // Convert to lowercase for predictability
        direction = direction.toLowerCase();
        // Determine whether User gave a valid direction
        if (directions.contains(direction)) {
            // Determine if that direction is not obstructed
            if (checkMovement(direction)) {
                // Move the User in the Map
                takeAMove(direction);
            } else {
                System.out.println("A wall blocks your way.");
                userMove();
            }
        } else {
            userMove();
        }
        // Display updated map
        myMap.printMap();
        // If haven't won, start process of moving over again
        if (!myMap.didIWin()) {
            userMove();
        } else {
            win();
        }
    }

    // Check if direction is obstructed and return a boolean
    public static boolean checkMovement(String direction) {
        if (direction.equals("r")) {
            return myMap.canIMoveRight();
        }
        if (direction.equals("l")) {
            return myMap.canIMoveLeft();
        }
        if (direction.equals("u")) {
            return myMap.canIMoveUp();
        } else { //direction.equals("d")
            return myMap.canIMoveDown();
        }
    }

    // Move User in chosen direction
    public static void takeAMove(String direction) {
        if (direction.equals("r")) {
            myMap.moveRight();
        }
        if (direction.equals("l")) {
            myMap.moveLeft();
        }
        if (direction.equals("u")) {
            myMap.moveUp();
        } else { //direction.equals("d")
            myMap.moveDown();
        }
    }

    //  PART 2
    // users only have 100 steps before the exit closes
    // keep track of user moves
    //movesMessage(moves) - takes int representing user's current number of moves + produces these messages when the user hits the following milestones:
    // number of moves -- message
    // 50 -- Warning: You have made 50 moves, you have 50 remaining before the maze exit closes
    // 75 -- Alert! You have made 75 moves, you only have 25 moves left to escape.
    // 90 -- DANGER! You have made 90 moves, you only have 10 moves left to escape!!
    // 100 -- Oh no! You took too long to escape, and now the maze exit is closed FOREVER >:[
    //if after 100 moves, if you still haven't escaped - end the game, tell the user they lost, allow program to exit
    // "Sorry, but you didn't escape in time- you lose!"
    //If they made it out before they run out of moves, add a line to the congratulations statement that tells them how many moves it took them
    // "and you did it in XX moves" (XX is the user's move count)

    // Part 3
    //The maze has pits. If the user moves onto a square with a pit they will fall and lose the game. Instead, you want to make sure that you are looking ahead and letting them know it there is a pit in front of them. If there is, give them the option to jump over it in the direction they were heading.
    //myMap.isThereAPit("R"); // takes in direction String the user entered in and returns if there is a pit ahead
    //myMap.jumpOverPit("L"); // Will jump over a pit in the direction given, skipping that space and landing 2 spaces over in the direction specified
    //all your code to handle a pit once it's been detected should be put into a separate method called navigatePit()
    //when you find a pit, use the following text to ask the user what to do next:
    // "Watch out! There's a pit ahead. Jump it?
    //The user should be able to enter in any string that starts with a "y." if they enter in anything else it will be assumed they do not want to jump the pit so they should be asked again
    // 'where would you like to move?'
}
