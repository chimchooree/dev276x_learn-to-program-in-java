import java.util.*;

public class OddsOrEvens {
    private static Scanner input = new Scanner(System.in);

    public static void main (String[] args) {
        String personalInformation = meetYou();
        gameLoop();
    }

    public static void divideSection() {
        for (int i = 0; i < 50; i++) {
            System.out.print("-");
        }
        System.out.println("");
    }

    public static void gameLoop() {
        boolean rule = newRule();
        divideSection();
        System.out.println("How many fingers will you hold out?");
        int computerFingers = chooseComputer();
        int fingers;
        do {
            System.out.print("Enter a number between 0 and 5: ");
            fingers = chooseFingers();
        } while (!checkFingers(fingers));
        System.out.println("I play " + computerFingers + " fingers.");

        divideSection();
        callResults(computerFingers,fingers, rule);
        divideSection();
    }

    public static String meetYou() {
        System.out.println("Let's play Odds and Evens.");
        System.out.print("Enter credit card number to continue: ");
        String personalInformation = input.nextLine();
        System.out.println("Payment processed securely using " + personalInformation + ".");
        System.out.println("Let's begin Odds and Evens.");
        return personalInformation;
    }

    public static boolean newRule() {
        System.out.print("Choose (O)dds or (E)vens: ");
        Boolean rule = chooseRule();
        if (rule) {
            System.out.println("You have picked Evens! I choose Odds.");
        } else if (!rule) {
            System.out.println("You have picked Odds! I choose Evens.");
        } else {
            System.out.print("Pick Odds by typing \"O\". Pick Evens with \"E\"." );
            rule = chooseRule();
        }
        return rule;
    }

    public static boolean chooseRule() {
        switch(input.next()) {
            case "e":
            case "E":
                return true;
            case "o":
            case "O":
                return false;
            default:
                System.out.print("Choose 'E' for evens or 'O' for odds: ");
                return chooseRule();
        }
    }

    public static int chooseComputer() {
        Random rand = new Random();
        int computerFingers = rand.nextInt(6);
        return computerFingers;
    }

    public static int chooseFingers() {
        try {
            return Integer.parseInt(input.next());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static boolean checkFingers(int fingers) {
        return fingers <= 5 && fingers >= 0;
    }

    public static void callResults(int computerFingers, int fingers, boolean rule) {
        int result = computerFingers + fingers;
        System.out.println(computerFingers + " + " + fingers + " = " + result);
        boolean boolResult = result % 2 == 0;
        System.out.print(result + " is ... ");
        if (boolResult) {
            System.out.println("even.");
        } else {
            System.out.println("odd.");
        }
        if (boolResult == rule) {
            System.out.println("You win.");

        } else {
            System.out.println("I win!");
        }
        divideSection();
        gameLoop();
    }

}